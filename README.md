# hdspinbot
A program which checks if any hard drives are currently spinning using `hdparm`.
If it detects any spinning devices, it will send a notification mail.

## Usage
This project requires `hdparm`. On Arch, install it with
```
pacman -S --needed hdparm
```
Edit the example config file `config.example.json` and save it as `config.json`.
Use `stack run` to run the program.
Please note that it exits after checking all hard drives and does not keep running.
Instead, it is recommended to set up a cronjob or systemd timer to execute this program regularly.

## License
```
hdspinbot
Copyright (C) 2024 tpart

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Main (main) where

import Data.Aeson
import Data.Aeson.TH
import Network.Mail.SMTP
import qualified Network.Mail.Mime as M
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as LT
import Control.Monad
import Turtle hiding (s,f,update)

configFile :: FilePath
configFile = "config.json"

data Config = Config {
        senderMail :: T.Text,
        receiverMail :: T.Text,
        smtpServer :: String,
        smtpUsername :: String,
        smtpPassword :: String
    } deriving (Show)
deriveJSON defaultOptions 'Config

main :: IO ()
main = do
    (_, out) <- procStrict "lsblk" ["--raw", "--output", "NAME"] mzero
    spins <- mapM checkSpinStatus $ filter (T.isPrefixOf "sd") $ T.lines out
    when (any isSpinning spins) $ do
        config <- decodeFileStrict configFile :: IO (Maybe Config)
        case config of
            Nothing -> T.putStrLn "invalid config"
            Just (Config sender receiver server username password) -> do
                let m = simpleMail (Address Nothing sender) [Address Nothing receiver] [] [] "Hard drive active" [M.plainPart $ LT.fromStrict "One or more hard drive is spinning!"]
                sendMailWithLoginTLS server username password m

checkSpinStatus :: Text -> IO Text
checkSpinStatus t = snd <$> procStrict "hdparm" ["-C", "/dev/" `T.append` t] mzero

isSpinning :: Text -> Bool
isSpinning = T.isInfixOf "active"